# Descripci�n #

Este proyecto fue  desarrollado para la asignatura de INTERACI�N PERSONA-ORDENADOR II,consiste en un juego implementado como una aplicacion de escritorio en C#.
A trav�s de este juego, podemos crear nuestro personaje al cual debemos de cuidar (darle de comer, evitar que se enfade, hacerle dormir), 
adem�s podemos mandar realizar estas acciones (hacerle dormir, darle de comer..) mediante comandos de voz.

En este juego, se incluye un minijuego (a parte de cuidar a nuestro personaje)en el que  se pueden ganar monedas (con las que 
podemos comprar nuevos trajes para nuestro personaje).

# Imagenes del Juego #


![](https://fotos.subefotos.com/286c73056780095717598a073ec6ea2eo.png)



![](https://fotos.subefotos.com/478ef66efd43155714fa3bb4e1accc25o.png)

**MiniJuego**

![](https://fotos.subefotos.com/e4647dbfc22a3289747a8deca1e38725o.png)

![](https://fotos.subefotos.com/5cb786f5d6f5527f286c5c3a174678efo.png)



