﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Threading;


namespace PracticaFinal
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer tReloj, tCuadroDialogo, tMovimiento, tJuego;

        private double intervalo;
        private int contador_comer;
        private int contador_dormir;
        private int segundos;
        private int monedas;
        private int contador_hola;
        private int nivel;
        private int dificultad;
        private int monedas_por_minuto;
        private int dificultad_juego; // 1 = facil,2 = medio, 3 = dificil
        private double px;

        private double[] px_niveles;

        private Boolean calzones_bajados;
        private Boolean enfadado;
        private Boolean texto_libre;
        private Boolean durmiendo;
        private Boolean movimiento_activo;
        private Boolean subtitulos;
        private Boolean cara_estado;
        private String nombre;

        private int puntos;
        

        private Point uno;

       

        //voz
        private SpeechRecognitionEngine voz = new SpeechRecognitionEngine();

        private SpeechSynthesizer synthesizer;

        private Storyboard pestaneo;

        private System.Media.SoundPlayer sonido = new System.Media.SoundPlayer { };
		

        private Boolean texto_hambre, texto_sueno;


        // VARIABLES DEL JUEGO
        private int segundos_restantes;

        public MainWindow()
		{
            this.InitializeComponent();
            gridInicio.Visibility = Visibility.Visible;
           
           
        } // Fin constructor


        //Pantalla de inicio

         //Con este método controlamos el botón "Empezar partida" de la pantalla de inicio, Cuando pulsemos en este boton, controlará si el nombre ha sido completado, si no lo  ha sido mostrará un mensaje de error en pantalla (diciendonos que es mecesario introducir un nombre). Si el nombre ha sido introducido, este boton pondrá el grid de pantalla de inicio oculto e iniciará el personaje.
        private void btnContinuar_Click(object sender, RoutedEventArgs e)
        {
			
			if(txtNombre.Text == ""){
				lblMensajeError.Visibility = Visibility.Visible;
			}
			else{
				gridInicio.Visibility = Visibility.Hidden;
            	nombre = txtNombre.Text;
            	inicializaPersonaje();
				lblMensajeError.Visibility = Visibility.Hidden;
			}
            

        }

        //En este método inicalizamos los temporizadores que usaremos en el juego,incializamos los puntos,e incializamos todo lo necesario para el establecimiento de la pantalla principal.
        public void inicializaPersonaje()
        {
            intervalo = 1000.0; // el reloj principal tendrá un intervalo de un segundo

            tReloj = new DispatcherTimer();
            tReloj.Interval = TimeSpan.FromMilliseconds(intervalo);
            tReloj.Tick += new EventHandler(reloj); // metodo que se ejecuta siempre (actualiza segundos por ejemplo)
            tReloj.Tick += new EventHandler(actualizaParametros); // metodo que actualiza el estado del juego

            tCuadroDialogo = new DispatcherTimer();
            tCuadroDialogo.Interval = TimeSpan.FromMilliseconds(4000.0); // los subtitulos duran 4 segundos

            tMovimiento = new DispatcherTimer(); // temporizador para controlar el movimiento

            //inicializacion de la puntuacion y de los puntos necesarios para ir pasando al siguiente nivel
            px = 0.0;
            nivel = 0;
            px_niveles = new double[17] {0.0, 100.0, 200.0, 350.0, 450.0, 600.0, 800.0, 1000.0, 1300.0, 1600.0, 2000.0,
                2500.0, 3000.0, 4000.0, 5000.0, 7000.0, 9000.0}; // px minimos necesarios para alcanzar el nivel

            // todas las barras empiezan a 0 (van aumentando) menos la de diversión
            PBapetito.Value = 0;
            PBenfado.Value = 0;
            PBsueno.Value = 0;
            PBDiversion.Value = 100;
			PBpx_nivel.Value = 0;


            texto_libre = true; // variable que controla si los subtitulos están libres
            calzones_bajados = false; // variable que controla si los calzones están bajados
            enfadado = false; // variable que controla si está enfadado
            durmiendo = false;// variable que controla si está durmiendo o no
            movimiento_activo = false; // variable que controla si se está ejecutando alguna animación
            texto_sueno = true; // variable que controla si puede decir tengo sueño o no
            texto_hambre = true;// variable que controla si puede decir tengo hambre o no
            subtitulos = true; // variable que controla si los subtitulos están activados o no
            cara_estado = false; // variable que controla si la cara está mostrando algun estado (sueño, hambre, enfado...)

            segundos = 0;
            monedas = 0;
            monedas_por_minuto = 5; // solo puede dar 5 monedas por minuto si habla o vuela el muñeco     
            dificultad = 5; // variable que controla como de rápido pueden subir las progress bar
            contador_comer = 4;
            contador_dormir = 10;
            contador_hola = 0; // contador que controla si se está moviendo o no el cursor

            uno = Mouse.GetPosition(CanvasGeneral);

            // voz del sistema
            synthesizer = new SpeechSynthesizer();
            synthesizer.SetOutputToDefaultAudioDevice();
           
            synthesizer.Volume = 100;
            synthesizer.Rate = 1;

            diFrase("Hola " + nombre+". ¡Espero que te lo pases genial!");
            tReloj.Start();


            // DATOS JUEGO

            segundos_restantes = 45;
            tJuego = new DispatcherTimer();
            tJuego.Interval = TimeSpan.FromMilliseconds(1000.0);



            //COLOR PERSONAJE
            e1_MouseLeftButtonDown(null, null);


            // establecimiento de la pantalla principal
            ImageBrush myBrush = new ImageBrush();
            Image image = new Image();
            image.Source = new BitmapImage(
                new Uri(AppDomain.CurrentDomain.BaseDirectory + "//Imagenes/imagen1.jpg"));
            myBrush.ImageSource = image.Source;
            Grid grid = new Grid();
            GridGeneral.Background = myBrush;

            // establecimiento de los campos iniciales
            lblNumMonedas.Content = 0;
            lblSegundos.Content = "00";
            lblMinutos.Content = "00";
            lblHora.Content = "00";
            lblNivel.Content = "Nivel 0";
            px = 0;
            rtbInformacion.IsReadOnly = true;
            rtbMensajeFinal.IsReadOnly = true;
			
			

            // establecimiento de todo lo que se ve al poner la pantalla
            Pizza.Visibility = Visibility.Hidden;
            ponerCaraNormal();
            e2.Opacity = 0.5;
            e2.IsEnabled = false;
            e3.Opacity = 0.5;
            e3.IsEnabled = false;
            e4.Opacity = 0.5;
            e4.IsEnabled = false;
            e5.Opacity = 0.5;
            e5.IsEnabled = false;
            lblSkin2.Opacity = 0.5;
            lblSkin3.Opacity = 0.5;
            lblSkin4.Opacity = 0.5;
            lblSkin5.Opacity = 0.5;
            Fondo2.Opacity = 0.5;
            Fondo2.IsEnabled = false;
            Fondo3.Opacity = 0.5;
            Fondo3.IsEnabled = false;
            Fondo4.Opacity = 0.5;
            Fondo4.IsEnabled = false;
            Fondo5.Opacity = 0.5;
            Fondo5.IsEnabled = false;
            lblFondo2.Opacity = 0.5;
            lblFondo3.Opacity = 0.5;
            lblFondo4.Opacity = 0.5;
            lblFondo5.Opacity = 0.5;
        }

        //////////////////////////////Temporizadores///////////////////////////////////////////////////////////////////////////


        private void reloj(object sender, EventArgs e) // metodo que se ejecuta siempre, cada segundo
        {
            segundos++;
            actualizaTiempo();

            if(segundos % 60 == 0)
            {
                monedas_por_minuto = 5;
            }
        }

         //Este método controla el funcionamiento del juego a lo largo del tiempo (comprueba estado, actualiza monedas,actualiza premios disponibles, sube los px cada 2 segundos y hace que cada minuto diga una frase.
        private void actualizaParametros(object sender, EventArgs e)
        {
            comprobarEstadoCorrecto();
            actualizarProgressBar();

            if (segundos % 10 == 0 && durmiendo == false)
            {
                pestanear();
            }

            posicion();

            // aumento de una moneda por tiempo superado cada 30 segundos
            if (segundos % 30 == 0)
            {
                actualizaMonedas(1);
            }

            // se actualizan los premios disponibles en caso de monedas suficientes
            actualizaPremios();

            if (segundos % 2 == 0)
            {
                subirPx(1); // cada 2 segundos que pasan se sube en uno los px
            }

            if (segundos % 60 == 0) // cada 60 segundos puede decir alguna cosa (consejos, curiosidades...)
            {
               
                switch (Dado(10))
                {
                    case 1:
                        diFrase("Puedes personalizan tanto a Superestrellaman como el fondo del juego en el apartado Personalización");
                        break;
                    case 2:
						diFrase("Juega a mi minijuego,diviertete con mi ayudante SuperEstrellaDog");
                        break;
                    case 3:
                        diFrase("Sé hacer muchas cosas, descubre todo lo que se hacer");
                        break;
                    case 4:
                        diFrase("Si te molestan los subtítulos, puedes quitarlos");
                        break;
                    case 5:
                        diFrase("Consigue monedas para acceder a los movimientos especiales");
                        break;
                    case 6:
                        diFrase("Si quieres conseguir monedas y subir de nivel más rapido, habla conmigo y juega al minijuego");
                        break;
                    case 7:
                        diFrase("¿Sabías que tengo un traje de oro? ");
                        break;
                    case 8:
                        diFrase("¡achus! uy he estornudado, tengo alergía");
                        break;
                    case 9:
                        diFrase("¿No te cansas de este fondo? ");
                        break;
                    case 10:
                        diFrase("Si me como una seta, me quedo como nuevo");
                        break;
                }
            }
        }

        //metodo que se ejecuta cuando está durmiendo, u funcionalidad es disminuir el cansancio de nuestro muñeco.
        private void dormir(object sender, EventArgs e)
        {
            this.PBsueno.Value -= 5; // se disminuye en 5 cada segundo, hasta 4 veces
            contador_dormir--;

            if (this.PBsueno.Value <= 0 || contador_dormir == 0)
            {
                tReloj.Tick += new EventHandler(actualizaParametros);
                tReloj.Tick -= new EventHandler(dormir);
                activarTodo();
                durmiendo = false;
                contador_dormir = 10;
                
                // se quita la cara en la que está durmiendo
                OjoIzq.Opacity = 1.0;
                OjoDer.Opacity = 1.0;
                PupilaIzq.Opacity = 1.0;
                PupilaDer.Opacity = 1.0;
                OjoIzqDurmiendo.Visibility = Visibility.Hidden;
                OjoDerDurmiendo.Visibility = Visibility.Hidden;
                Boca.Visibility = Visibility.Visible;
                bocaDurmiendo.Visibility = Visibility.Hidden;
            }
        }



        //metodo que se ejecuta cuando está comiendo, se disminuye el apetito que tiene SuperestrellaMan.

        private void comer(object sender, EventArgs e)
        {
            if (contador_comer == 5)
            {
                come_pizza();
            }

            this.PBapetito.Value -= 10; // se reduce en 10 cada segundo que come
            contador_comer--;


            if (contador_comer == 0)
            {
                tReloj.Tick += new EventHandler(actualizaParametros);
                tReloj.Tick -= new EventHandler(comer);
                activarTodo();
                contador_comer = 4;
            }

        }

        // metodo que quita los subtitulos que estén activados cuando acaba el temporizador
        private void cuadroDialogo(object sender, EventArgs e)
        {
            //Texto.Opacity = 0;
			Texto.Visibility = Visibility.Hidden;
            Texto.Text = "";
            texto_libre = true;
            tCuadroDialogo.Tick -= new EventHandler(cuadroDialogo);
            tCuadroDialogo.Stop();
        }

        //Este metodo es el encargado de activar todos los elementos de nuevo una vez haya finalizado alguna animación.
        private void tempMovimiento(object sender, EventArgs e)
        { 
            movimiento_activo = false;

            //se activan todas las opciones y el temporizador de actualizacion de parametros
            activarTodo();
            tReloj.Tick += new EventHandler(actualizaParametros);

            // se para el temporizador de movimiento
            tMovimiento.Tick -= new EventHandler(tempMovimiento);
            tMovimiento.Stop();
        }

        //Este método activa el temporizador de movimiento, para ello se le pasan los minisegundos que dura la animación.
        private void empezarTemporizadorMovimiento(double milisegundos) 
        {

            desactivarTodo(); // se desactiva todo para que no se pueda empezar otra animacion
            tReloj.Tick -= new EventHandler(actualizaParametros); // se quita el temporizador de actualizacion de parametros

            tMovimiento.Interval = TimeSpan.FromMilliseconds(milisegundos);
            tMovimiento.Tick += new EventHandler(tempMovimiento);
            tMovimiento.Start();
            movimiento_activo = true;
        }

        //////////////////////////////  Oyentes  ///////////////////////////////////////////////////////////////////////////

        private void ActivarVoz(object sender, RoutedEventArgs e) // oyente del botón de activar la voz
        {
            voz.RecognizeAsync(RecognizeMode.Multiple); // se empieza a escuchar lo que dice el usuario
            btnActivarVoz.IsEnabled = false;
            btnDesactivarVoz.IsEnabled = true;
        }

        private void btnDesactivarVoz_Click(object sender, RoutedEventArgs e) // oyente del botón de desactivar la voz
        {
            voz.RecognizeAsyncStop();// se termina de escuchar lo que dice el usuario
            btnDesactivarVoz.IsEnabled = false;
            btnActivarVoz.IsEnabled = true;
        }

        // oyente del icono del movimiento especial de las palmas
		private void BotonPalmas_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			CvPersonalizacion.Visibility = Visibility.Hidden;
            animacionPalmas();
            
            
        }
        // oyente del icono del movimiento especial de los toques del balon
        private void BotonBalon_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			CvPersonalizacion.Visibility = Visibility.Hidden;
			toquesBalon();
            
        }
        // oyente del icono del movimiento especial del baile
        private void BotonNota_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			CvPersonalizacion.Visibility = Visibility.Hidden;            
            animacionBailar();
        }
        // oyente del icono del movimiento especial del troll
        private void BotonTroll_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			CvPersonalizacion.Visibility = Visibility.Hidden;
            animacionTroll();
            
        }
        // oyente del icono del movimiento especial del champiñon
        private void BotonChampinon_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			CvPersonalizacion.Visibility = Visibility.Hidden;
            animacionChampinon();
        }
        // oyente del icono del movimiento especial del frisbee
        private void BotonFrisbee_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			CvPersonalizacion.Visibility = Visibility.Hidden;
            animacionFrisbee();
        }

        // oyente del botón de movimientos especiales
        private void MenuMovimiento(object sender, RoutedEventArgs e)
        {
            if (CvMovimientos_Especiales.Visibility == Visibility.Hidden)
            {
                CvMovimientos_Especiales.Visibility = Visibility.Visible;
            }
            else
            {
                CvMovimientos_Especiales.Visibility = Visibility.Hidden;
            }

            CvPersonalizacion.Visibility = Visibility.Hidden;
        }
        // oyente del botón de la personalización
        private void MenuPersonalizacion(object sender, RoutedEventArgs e)
        {
            if (CvPersonalizacion.Visibility == Visibility.Hidden)
            {
                CvPersonalizacion.Visibility = Visibility.Visible;
            }
            else
            {
                CvPersonalizacion.Visibility = Visibility.Hidden;
            }

            CvMovimientos_Especiales.Visibility = Visibility.Hidden;
        }
        // en caso de pnchar sobre el layour general quitar los 2 menus desplegables si estuvieran visibles
        private void click_LayoutRoot(object sender, MouseButtonEventArgs e)
        {
            CvMovimientos_Especiales.Visibility = Visibility.Hidden;
            CvPersonalizacion.Visibility = Visibility.Hidden;
        }
        // oyente del icono dormir
        private void duerme_click(object sender, MouseButtonEventArgs e)
        {
            empezarDormir();
        }
        // oyente de la estrella del personaje
        private void Tocar_Estrella(object sender, MouseButtonEventArgs e)
        {
            Volar(); // al tocar la estrella vuela
        }
        // oyente de los calzones del personaje
        private void Bajar_Calzones(object sender, MouseButtonEventArgs e)
        {
            bajarCalzones();// al tocar los calzones estos se bajan
        }
        // oyente de los ojos del personaje
        private void tocar_ojo(object sender, MouseButtonEventArgs e)
        {
            this.PBenfado.Value += 5; // al tocar el ojo sube su enfado
        }

        // oyente del icono de la pizza (empieza a comer)
        private void comer_click(object sender, MouseButtonEventArgs e)
        {
            empezarComer();
        }

        // oyente del boton para volver a empezar cuando ha muerto
        private void btnVolverEmpezar_Click(object sender, RoutedEventArgs e)
        {
            gridInicio.Visibility = Visibility.Visible;
            txtNombre.Text = "";

            activarTodo();
            gridFinal.Visibility = Visibility.Hidden;
        }

        // oyente del boton para finalizar el juego cuando ha muerto
        private void btnFinalizar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        // oyente de la skin 1
        private void e1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            LinearGradientBrush myVerticalGradient1 = new LinearGradientBrush();
            myVerticalGradient1.StartPoint = new Point(0.5, 0);
            myVerticalGradient1.EndPoint = new Point(0.5, 1);
            myVerticalGradient1.GradientStops.Add(
                new GradientStop(Colors.Blue, 0.0));
            myVerticalGradient1.GradientStops.Add(
                new GradientStop(Colors.Black, 1.0));

            LinearGradientBrush myVerticalGradient2 = new LinearGradientBrush();
            myVerticalGradient2.StartPoint = new Point(0.5, 0);
            myVerticalGradient2.EndPoint = new Point(0.5, 1);
            myVerticalGradient2.GradientStops.Add(
                new GradientStop(Colors.Black, 0.0));
            myVerticalGradient2.GradientStops.Add(
                new GradientStop(Colors.Blue, 1));
			
			LinearGradientBrush myVerticalGradient3 = new LinearGradientBrush();
            myVerticalGradient3.StartPoint = new Point(0.5, 0);
            myVerticalGradient3.EndPoint = new Point(0.5, 1);
            myVerticalGradient3.GradientStops.Add(
                new GradientStop(Colors.Black, 0.0));
            myVerticalGradient3.GradientStops.Add(
                new GradientStop(Colors.Red, 1));


            Cuerpo.Fill = myVerticalGradient2;
            Capa.Fill = myVerticalGradient3;
            FondoEstrella.Fill = new SolidColorBrush(Colors.Black);
            Calzones.Fill = new SolidColorBrush(Colors.Black);

            FemurDer.Fill = myVerticalGradient1;
            FemurIzq.Fill = myVerticalGradient1;
            RodillaDer.Fill = myVerticalGradient3;
            RodillaIzq.Fill = myVerticalGradient3;
            EspinillaDer.Fill = myVerticalGradient1;
            EspinillaIzq.Fill = myVerticalGradient1;
            PieDer.Fill = new SolidColorBrush(Colors.Red);
            PieIzq.Fill = new SolidColorBrush(Colors.Red);

            HombroIzq.Fill = new SolidColorBrush(Colors.Black);
            HombroDer.Fill = new SolidColorBrush(Colors.Black);
            BrazoIzq.Fill = new SolidColorBrush(Colors.Black);
            BrazoDer.Fill = new SolidColorBrush(Colors.Black);
            CodoDer.Fill = new SolidColorBrush(Colors.Blue);
            CodoIzq.Fill = new SolidColorBrush(Colors.Blue);
            AntebrazoIzq.Fill = new SolidColorBrush(Colors.Black);
            AntebrazoDer.Fill = new SolidColorBrush(Colors.Black);
        }

        // oyente de la skin 2
        private void e2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            LinearGradientBrush myVerticalGradient = new LinearGradientBrush();
            myVerticalGradient.StartPoint = new Point(0.5, 0);
            myVerticalGradient.EndPoint = new Point(0.5, 1);
            myVerticalGradient.GradientStops.Add(new GradientStop(Colors.Green, 0.0));
            //myVerticalGradient.GradientStops.Add(new GradientStop(Colors.Black, 1.0));

            Cuerpo.Fill = myVerticalGradient;
            Capa.Fill = new SolidColorBrush(Colors.White);
            FondoEstrella.Fill = new SolidColorBrush(Colors.White);
            Calzones.Fill = new SolidColorBrush(Colors.White);

            FemurDer.Fill = myVerticalGradient;
            FemurIzq.Fill = myVerticalGradient;
            RodillaDer.Fill = new SolidColorBrush(Colors.White);
            RodillaIzq.Fill = new SolidColorBrush(Colors.White);
            EspinillaDer.Fill = myVerticalGradient;
            EspinillaIzq.Fill = myVerticalGradient;
            PieDer.Fill = new SolidColorBrush(Colors.White);
            PieIzq.Fill = new SolidColorBrush(Colors.White);

            HombroIzq.Fill = new SolidColorBrush(Colors.White);
            HombroDer.Fill = new SolidColorBrush(Colors.White);
            BrazoIzq.Fill = new SolidColorBrush(Colors.White);
            BrazoDer.Fill = new SolidColorBrush(Colors.White);
            CodoDer.Fill = myVerticalGradient;
            CodoIzq.Fill = myVerticalGradient;
            AntebrazoIzq.Fill = new SolidColorBrush(Colors.White);
            AntebrazoDer.Fill = new SolidColorBrush(Colors.White);
        }

        // oyente de la skin 3
        private void e3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            LinearGradientBrush myLinearGradientBrush = new LinearGradientBrush();
            myLinearGradientBrush.StartPoint = new Point(0, 0.5);
            myLinearGradientBrush.EndPoint = new Point(1, 0.5);
            myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.Red, 0.0));
            myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.White, 0.35));
            myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.Red, 0.5));
            myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.White, 0.65));
            myLinearGradientBrush.GradientStops.Add(new GradientStop(Colors.Red, 1));

            Cuerpo.Fill = myLinearGradientBrush;
            Capa.Fill = new SolidColorBrush(Colors.Red);
            FondoEstrella.Fill = new SolidColorBrush(Colors.Black);
            Calzones.Fill = new SolidColorBrush(Colors.Black);

            FemurDer.Fill = new SolidColorBrush(Colors.Black);
            FemurIzq.Fill = new SolidColorBrush(Colors.Black);
            RodillaDer.Fill = new SolidColorBrush(Colors.Red);
            RodillaIzq.Fill = new SolidColorBrush(Colors.Red);
            EspinillaDer.Fill = new SolidColorBrush(Colors.Black);
            EspinillaIzq.Fill = new SolidColorBrush(Colors.Black);
            PieDer.Fill = new SolidColorBrush(Colors.Red);
            PieIzq.Fill = new SolidColorBrush(Colors.Red);

            HombroIzq.Fill = new SolidColorBrush(Colors.Black);
            HombroDer.Fill = new SolidColorBrush(Colors.Black);
            BrazoIzq.Fill = new SolidColorBrush(Colors.Black);
            BrazoDer.Fill = new SolidColorBrush(Colors.Black);
            CodoDer.Fill = new SolidColorBrush(Colors.White);
            CodoIzq.Fill = new SolidColorBrush(Colors.White);
            AntebrazoIzq.Fill = new SolidColorBrush(Colors.Black);
            AntebrazoDer.Fill = new SolidColorBrush(Colors.Black);
        }
        
        // oyente de la skin 4
        private void e4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            Cuerpo.Fill = new SolidColorBrush(Colors.Black);
            Capa.Fill = new SolidColorBrush(Colors.Red);
            FondoEstrella.Fill = new SolidColorBrush(Colors.Red);
            Calzones.Fill = new SolidColorBrush(Colors.Red);

            FemurDer.Fill = new SolidColorBrush(Colors.Black);
            FemurIzq.Fill = new SolidColorBrush(Colors.Black);
            RodillaDer.Fill = new SolidColorBrush(Colors.Red);
            RodillaIzq.Fill = new SolidColorBrush(Colors.Red);
            EspinillaDer.Fill = new SolidColorBrush(Colors.Black);
            EspinillaIzq.Fill = new SolidColorBrush(Colors.Black);
            PieDer.Fill = new SolidColorBrush(Colors.Red);
            PieIzq.Fill = new SolidColorBrush(Colors.Red);

            HombroIzq.Fill = new SolidColorBrush(Colors.Red);
            HombroDer.Fill = new SolidColorBrush(Colors.Red);
            BrazoIzq.Fill = new SolidColorBrush(Colors.Red);
            BrazoDer.Fill = new SolidColorBrush(Colors.Red);
            CodoDer.Fill = new SolidColorBrush(Colors.Black);
            CodoIzq.Fill = new SolidColorBrush(Colors.Black);
            AntebrazoIzq.Fill = new SolidColorBrush(Colors.Red);
            AntebrazoDer.Fill = new SolidColorBrush(Colors.Red);
        }

        // oyente de la skin 5
        private void e5_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            Cuerpo.Fill = new SolidColorBrush(Colors.Gold);
            Capa.Fill = new SolidColorBrush(Colors.Black);
            FondoEstrella.Fill = new SolidColorBrush(Colors.Black);
            Calzones.Fill = new SolidColorBrush(Colors.Black);

            FemurDer.Fill = new SolidColorBrush(Colors.Gold);
            FemurIzq.Fill = new SolidColorBrush(Colors.Gold);
            RodillaDer.Fill = new SolidColorBrush(Colors.Black);
            RodillaIzq.Fill = new SolidColorBrush(Colors.Black);
            EspinillaDer.Fill = new SolidColorBrush(Colors.Gold);
            EspinillaIzq.Fill = new SolidColorBrush(Colors.Gold);
            PieDer.Fill = new SolidColorBrush(Colors.Black);
            PieIzq.Fill = new SolidColorBrush(Colors.Black);

            HombroIzq.Fill = new SolidColorBrush(Colors.Black);
            HombroDer.Fill = new SolidColorBrush(Colors.Black);
            BrazoIzq.Fill = new SolidColorBrush(Colors.Black);
            BrazoDer.Fill = new SolidColorBrush(Colors.Black);
            CodoDer.Fill = new SolidColorBrush(Colors.Gold);
            CodoIzq.Fill = new SolidColorBrush(Colors.Gold);
            AntebrazoIzq.Fill = new SolidColorBrush(Colors.Black);
            AntebrazoDer.Fill = new SolidColorBrush(Colors.Black);
        }

        // oyente del fondo 1
        private void Fondo1_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            ImageBrush myBrush = new ImageBrush();
            Image image = new Image();
            image.Source = new BitmapImage(
                new Uri(AppDomain.CurrentDomain.BaseDirectory + "//Imagenes/imagen1.jpg"));
            myBrush.ImageSource = image.Source;
            Grid grid = new Grid();
            GridGeneral.Background = myBrush;
        }

        // oyente del fondo 2
        private void Fondo2_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            ImageBrush myBrush = new ImageBrush();
            Image image = new Image();
            image.Source = new BitmapImage(
                new Uri(AppDomain.CurrentDomain.BaseDirectory + "//Imagenes/imagen2.jpg"));
            myBrush.ImageSource = image.Source;
            Grid grid = new Grid();
            GridGeneral.Background = myBrush;
        }

        // oyente del fondo 3
        private void Fondo3_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            ImageBrush myBrush = new ImageBrush();
            Image image = new Image();
            image.Source = new BitmapImage(
                new Uri(AppDomain.CurrentDomain.BaseDirectory + "//Imagenes/imagen3.jpg"));
            myBrush.ImageSource = image.Source;
            Grid grid = new Grid();
            GridGeneral.Background = myBrush;
        }

        // oyente del fondo 4
        private void Fondo4_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            ImageBrush myBrush = new ImageBrush();
            Image image = new Image();
            image.Source = new BitmapImage(
                new Uri(AppDomain.CurrentDomain.BaseDirectory + "//Imagenes/imagen4.jpg"));
            myBrush.ImageSource = image.Source;
            Grid grid = new Grid();
            GridGeneral.Background = myBrush;
        }

        // oyente del fondo 5
        private void Fondo5_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            CvPersonalizacion.Visibility = Visibility.Hidden;

            ImageBrush myBrush = new ImageBrush();
            Image image = new Image();
            image.Source = new BitmapImage(
                new Uri(AppDomain.CurrentDomain.BaseDirectory + "//Imagenes/imagen5.jpg"));
            myBrush.ImageSource = image.Source;
            Grid grid = new Grid();
            GridGeneral.Background = myBrush;
        }

        // oyente del boton de subtitulos
        private void btnSubtitulos_Click(object sender, RoutedEventArgs e)
        {
            if (subtitulos == true) // si estan activados se desactivan
            {
                subtitulos = false;
                ImageBrush myBrush = new ImageBrush();
                Image image = new Image();
                image.Source = new BitmapImage(
                    new Uri(AppDomain.CurrentDomain.BaseDirectory + "//Imagenes/subitulosDesactivados.jpg"));
                myBrush.ImageSource = image.Source;
                //Grid grid = new Grid();
                btnSubtitulos.Background = myBrush;

                diFrase("Subtítulos desactivados");
            }
            else // si estan desactivados se activan
            {
                subtitulos = true;
                ImageBrush myBrush = new ImageBrush();
                Image image = new Image();
                image.Source = new BitmapImage(
                    new Uri(AppDomain.CurrentDomain.BaseDirectory + "//Imagenes/subitulosActivado.jpg"));
                myBrush.ImageSource = image.Source;
                //Grid grid = new Grid();
                btnSubtitulos.Background = myBrush;

                diFrase("Subtítulos activados");
            }
        }

        ////////////////////////////// Actualizadores ///////////////////////////////////////////////////////////////////////////

        //Este método pone la apariencia de que nuestro superEstrellaMan ha muerto.
        private void cambioMuerto(){
			OjoIzq.Visibility = Visibility.Hidden;
			OjoDer.Visibility = Visibility.Hidden;
			OjoIzqSueno.Visibility = Visibility.Hidden;
            OjoDerSueno.Visibility = Visibility.Hidden;
            OjeraIzq.Visibility = Visibility.Hidden;
			OjeraDer.Visibility = Visibility.Hidden;
			
			
			OjoIzqMuerto.Visibility = Visibility.Visible;
			OjoDerMuerto.Visibility = Visibility.Visible;
		}

        //Este método comprueba si el muñeco ha muerto o si está en mal estado para notificar al usuario ya sea por voz o por cuadro de diálogo.
        private void comprobarEstadoCorrecto() 
        {
            // se comprueba primero si ha muerto (dependiendo de la muerte muestra un mensaje distinto)
            if (PBapetito.Value >= 100) // ha muerto por hambre
            {
                tReloj.Stop();
                desactivarTodo();
				cambioMuerto();
                gridFinal.Visibility = Visibility.Visible;
                tbMensajeMuerto.Text = "¡Has muerto debido a la hambre que tenia SuperEstrellaMan!\nPuntuacion: " + px;
                sonido.Stream = Properties.Resources.GameOver;
                sonido.Play();
            }
            else if (PBDiversion.Value <= 0) // ha muerto por falta de diversion
            {
                tReloj.Stop();
                desactivarTodo();
				cambioMuerto();
                gridFinal.Visibility = Visibility.Visible;
                tbMensajeMuerto.Text = "¡Has muerto debido a la falta de diversion que tenia SuperEstrellaMan!\nPuntuacion: " + px;
                sonido.Stream = Properties.Resources.GameOver;
                sonido.Play();
            }
            else if(PBenfado.Value >= 100) // ha muerto por enfado
            {
                tReloj.Stop();
                desactivarTodo();
				cambioMuerto();
                gridFinal.Visibility = Visibility.Visible;
                tbMensajeMuerto.Text = "¡Has muerto porque se ha enfadado mucho SuperEstrellaMan!\nPuntuacion: " + px;
                sonido.Stream = Properties.Resources.GameOver;
                sonido.Play();
            }
            else if (PBsueno.Value >= 100) // ha muerto por sueño
            {
                tReloj.Stop();
                desactivarTodo();
				cambioMuerto();
                gridFinal.Visibility = Visibility.Visible;
                tbMensajeMuerto.Text = "¡Has muerto debido a que SuperEstrellaMan necesitaba dormir!\nPuntuacion: " + px;
                sonido.Stream = Properties.Resources.GameOver;
                sonido.Play();


            }

            else // si no ha muerto se comprueban valores por si se necesita mostrar algun mensaje
            {
                if (PBapetito.Value > 75)  // tiene hambre
                {
                    // si no esta ni durmiendo ni enfadado se cambia la cara
                    if (!cara_estado)
                    {
                        cara_estado = true;
                        Boca.Visibility = Visibility.Hidden;
                        BocaHambre.Visibility = Visibility.Visible;
                    }

                    if (texto_hambre && texto_libre)
                    {
                        diFrase("Tengo hambre");
						tocaTripa();
                        texto_hambre = false;
                    } 
                }

                if (PBsueno.Value > 75)  // tiene sueño
                {
                    if (!cara_estado)
                    {
                        cara_estado = true;
                        OjoIzq.Opacity = 0.0;
                        OjoDer.Opacity = 0.0;
                        OjoIzqSueno.Visibility = Visibility.Visible;
                        OjoDerSueno.Visibility = Visibility.Visible;
                        OjeraIzq.Visibility = Visibility.Visible;
                        OjeraDer.Visibility = Visibility.Visible;
                    }

                    if (texto_sueno && texto_libre)
                    {
                        diFrase("Tengo sueño");
                        texto_sueno = false;
                    } 
                }

                if (this.PBenfado.Value > 60) // esta enfadado
                {
                    if (!cara_estado)
                    {
                        cara_estado = true;
                        enfadado = true;
                        OjoDer.Fill = new SolidColorBrush(Colors.Red);
                        OjoIzq.Fill = new SolidColorBrush(Colors.Red);
                        Boca.Visibility = Visibility.Hidden;
                        BocaEnfadado.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    if (enfadado)
                    {
                        enfadado = false;
                        cara_estado = false;
                        OjoDer.Fill = new SolidColorBrush(Colors.White);
                        OjoIzq.Fill = new SolidColorBrush(Colors.White);
                        Boca.Visibility = Visibility.Visible;
                        BocaEnfadado.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        //Este método actualiza el tiempo, su funcion es mostrarnos el tiempo que llevamos jugando.
        private void actualizaTiempo()  
        {
            int segundos;
            if (lblSegundos.Content.Equals("59"))
            {
                lblSegundos.Content = "00";

                if (lblMinutos.Content.Equals("59"))
                {
                    lblMinutos.Content = "00";

                    int horas = Convert.ToInt32(lblHora.Content) + 1;

                    if (horas < 10)
                    {
                        lblHora.Content = '0' + horas.ToString();
                    }
                    else
                    {
                        lblHora.Content = horas.ToString();
                    }
                }
                else
                {
                    int minutos = Convert.ToInt32(lblMinutos.Content) + 1;

                    if (minutos < 10)
                    {
                        lblMinutos.Content = '0' + minutos.ToString();
                    }
                    else
                    {
                        lblMinutos.Content = minutos.ToString();
                    }
                }
            }
            else
            {
                segundos = Convert.ToInt32(lblSegundos.Content) + 1;
                if (segundos < 10)
                {
                    lblSegundos.Content = '0' + segundos.ToString();
                }
                else
                {
                    lblSegundos.Content = segundos.ToString();
                }
            }
        }


        private void subirPx(int n) // metodo para subir los px 
        {
            px += n; // se suben los px

            // se comprueba si se sube de nivel (se sube si los px superan al minimo de px del siguiente nivel)
            if(nivel != 16)
            {
                if (px >= px_niveles[nivel + 1])
                {
                    subeNivel();
                }
                double barra = ((px - px_niveles[nivel]) / (px_niveles[nivel + 1] - px_niveles[nivel])) * 100;
                PBpx_nivel.Value = barra;
            }
            
        }

        //Este método hace que cuando subamos de nivel,nos muestre por pantalla el nivel en el que estamos, suene un sonido para enterarnos de que hemos alcanzado otro nivel y  dependiendo del nivel la dificultad va siendo mayor.
        private void subeNivel()
        {
            nivel++;
            lblNivel.Content = "Nivel " + nivel.ToString();

            sonido.Stream = Properties.Resources.nivelCompletado;
            sonido.Play();

            switch (nivel) // dependiendo del nivel se le dan una cantidad de monedas y puede desbloquear cosas de personalizacion
            {
				
                case 1:
                    dificultad = 6;
					
					// se desbloquea el segundo fondo 
					lblBloqueado2.Visibility = Visibility.Hidden;
					Fondo2.Opacity = 1.0;
                    Fondo2.IsEnabled = true;
                    lblFondo2.Opacity = 1.0;
                    actualizaMonedas(10);                   

                    diFrase("¡Felicidades! Has subido al nivel " + nivel + ". Has desbloqueado un nuevo fondo");
                    break;
                case 2:
                    dificultad = 7;

                    // se desbloquea la segunda skin 
                    lblBloqueado1.Visibility = Visibility.Hidden;
                    e2.Opacity = 1.0;
                    e2.IsEnabled = true;
                    lblSkin2.Opacity = 1.0;
                    actualizaMonedas(12);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel + ". Has desbloqueado una nueva skin");
                    break;
                case 3:
                    dificultad = 8;
					actualizaMonedas(14);
					
                    diFrase("¡Felicidades! Has subido al nivel " + nivel);
                    break;
                case 4:
                    dificultad = 9;

                    // se desbloquea el tercer fondo 
                    lblBloqueado3.Visibility = Visibility.Hidden;
                    Fondo3.Opacity = 1.0;
                    Fondo3.IsEnabled = true;
                    lblFondo3.Opacity = 1.0;
                    actualizaMonedas(17);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel + ". Has desbloqueado un nuevo fondo");
                    break;
                case 5:
                    dificultad = 10;

                    // se desbloquea la tercera skin 
                    lblBloqueado4.Visibility = Visibility.Hidden;
                    e3.Opacity = 1.0;
                    e3.IsEnabled = true;
                    lblSkin3.Opacity = 1.0;
                    actualizaMonedas(20);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel + ". Has desbloqueado una nueva skin");
                    break;
                case 6:
                    dificultad = 11;

                    actualizaMonedas(24);
                    sonido.Stream = Properties.Resources.nivelCompletado;
                    sonido.Play();
                    diFrase("¡Felicidades! Has subido al nivel " + nivel);
                    break;
                case 7:
                    dificultad = 12;

                    // se desbloquea el cuarto fondo 
                    lblBloqueado7.Visibility = Visibility.Hidden;
                    Fondo4.Opacity = 1.0;
                    Fondo4.IsEnabled = true;
                    lblFondo4.Opacity = 1.0;
                    actualizaMonedas(28);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel + ". Has desbloqueado un nuevo fondo");
                    break;
                case 8:
                    dificultad = 14;

                    actualizaMonedas(35);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel);
                    break;
                case 9:
                    dificultad = 16;

                    actualizaMonedas(45);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel);
                    break;
                case 10:
                    dificultad = 18;

                    // se desbloquea la cuarta skin 
                    lblBloqueado5.Visibility = Visibility.Hidden;
                    e4.Opacity = 1.0;
                    e4.IsEnabled = true;
                    lblSkin4.Opacity = 1.0;
                    actualizaMonedas(60);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel + ". Has desbloqueado una nueva skin");
                    break;
                case 11:
                    dificultad = 20;

                    actualizaMonedas(90);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel);
                    break;
                case 12:
                    dificultad = 23;

                    actualizaMonedas(120);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel);
                    break;
                case 13:
                    dificultad = 26;

                    // se desbloquea el quinto fondo 
                    lblBloqueado8.Visibility = Visibility.Hidden;
                    Fondo5.Opacity = 1.0;
                    Fondo5.IsEnabled = true;
                    lblFondo5.Opacity = 1.0;
                    actualizaMonedas(150);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel + ". Has desbloqueado un nuevo fondo");
                    break;
                case 14:
                    dificultad = 30;

                    actualizaMonedas(200);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel);
                    break;
                case 15:
                    dificultad = 33;

                    // se desbloquea la quinta skin 
                    lblBloqueado6.Visibility = Visibility.Hidden;
                    e5.Opacity = 1.0;
                    e5.IsEnabled = true;
                    lblSkin5.Opacity = 1.0;
                    actualizaMonedas(250);

                    diFrase("¡Felicidades! Has subido al nivel " + nivel + ". Has desbloqueado una nueva skin");
                    break;
				case 16:
					dificultad = 60;
                    actualizaMonedas(500);
                    PBpx_nivel.Value = 100;

                    diFrase("¡Felicidades! Has subido al nivel " + nivel + ". Este es el ultimo nivel, un gran desafío." + 
					 "¡A ver cuanto consigues aguantar!");
					break;
            }
        }

        //Este método habilita los premios dependiendo del número de monedas que tengamos.
        private void actualizaPremios()
        {
            if(monedas >= 100)
            {
                BotonChampinon.Opacity = 1.0;
                BotonNota.Opacity = 1.0;
                BotonTroll.Opacity = 1.0;
                BotonFrisbee.Opacity = 1.0;
                BotonBalon.Opacity = 1.0;
                BotonPalmas.Opacity = 1.0;
            }else if(monedas >= 75)
            {
                BotonChampinon.Opacity = 0.5;
                BotonNota.Opacity = 1.0;
                BotonTroll.Opacity = 1.0;
                BotonFrisbee.Opacity = 1.0;
                BotonBalon.Opacity = 1.0;
                BotonPalmas.Opacity = 1.0;
            }
            else if (monedas >= 50)
            {
                BotonChampinon.Opacity = 0.5;
                BotonNota.Opacity = 0.5;
                BotonTroll.Opacity = 1.0;
                BotonFrisbee.Opacity = 1.0;
                BotonBalon.Opacity = 1.0;
                BotonPalmas.Opacity = 1.0;
            }
            else if (monedas >= 35)
            {
                BotonChampinon.Opacity = 0.5;
                BotonNota.Opacity = 0.5;
                BotonTroll.Opacity = 0.5;
                BotonFrisbee.Opacity = 1.0;
                BotonBalon.Opacity = 1.0;
                BotonPalmas.Opacity = 1.0;
            }
            else if (monedas >= 20)
            {
                BotonChampinon.Opacity = 0.5;
                BotonNota.Opacity = 0.5;
                BotonTroll.Opacity = 0.5;
                BotonFrisbee.Opacity = 0.5;
                BotonBalon.Opacity = 1.0;
                BotonPalmas.Opacity = 1.0;
            }
            else if (monedas >= 10)
            {
                BotonChampinon.Opacity = 0.5;
                BotonNota.Opacity = 0.5;
                BotonTroll.Opacity = 0.5;
                BotonFrisbee.Opacity = 0.5;
                BotonBalon.Opacity = 0.5;
                BotonPalmas.Opacity = 1.0;
            }
            else 
            {
                BotonChampinon.Opacity = 0.5;
                BotonNota.Opacity = 0.5;
                BotonTroll.Opacity = 0.5;
                BotonFrisbee.Opacity = 0.5;
                BotonBalon.Opacity = 0.5;
                BotonPalmas.Opacity = 0.5;
            }
        }

        //Este método suma las monedas actuales mas el numero que le pasemos, controlamos que no haya monedas negativas
        private void actualizaMonedas(int num)
        {
                monedas += num;
            if (monedas < 0) //Si las monedas dan resultado negativo, se ponen a 0.
            {
                monedas = 0;
            }
                lblNumMonedas.Content = monedas.ToString();
            
           
            
        }
        
        //Método que actualiza las distintas progressbar
        private void actualizarProgressBar() 
        {
            // se sube en función de la dificultad (de manera aleatoria)
            this.PBapetito.Value += this.Dado(dificultad); 
			this.PBsueno.Value += this.Dado(dificultad);
			
			if (segundos % 3 == 0) // cada 4 segundos disminuye en 1 la barra de diversion
            {
                this.PBDiversion.Value -= Dado(dificultad/2);
            }
			
			if(calzones_bajados){ // si los pantalones estan bajados sube su enfado en 5
				PBenfado.Value += 5;
                subirPx(10);
			}else{
				this.PBenfado.Value -= 1;
			}
				
            

            if (this.PBapetito.Value > 30)
            {
                //cvIconoPizza.IsEnabled = true;
                cvIconoPizza.Opacity = 1.0;
            }
            else
            {
                    //cvIconoPizza.IsEnabled = false;
                cvIconoPizza.Opacity = 0.5;
            }

            if (this.PBsueno.Value > 30)
            {
               //CvIconoDormir.IsEnabled = true;
                CvIconoDormir.Opacity = 1.0;
            }
            else
            {
               //CvIconoDormir.IsEnabled = false;
               CvIconoDormir.Opacity = 0.5;
            }

            
            
            
        }
         
          //Este método controla posicion del puntero del ratón del jugador, si la posición de este no varía en 20 segundos, el muñeco nos dice que le hagamos caso
        private void posicion()
        {
            Point dos = Mouse.GetPosition(CanvasGeneral);


            if (uno == dos)
            {
                contador_hola++;
                if (contador_hola == 20)
                {
                    diFrase("Hazme caso");
                }
            }
            else { // ha cambiado de posicion
                uno = dos;
                contador_hola = 0;
            }
        }
        //////////////////////////////  Metodos auxiliares  ///////////////////////////////////////////////////////////

            //Este método crea números aleatorios
        private int Dado(int max)
        {
            Random dado = new Random();
			//Random dado = new Random(DateTime.Now.Millisecond);
            return 1 + dado.Next(max);
        }

        //Este método lo usamos para poner la cara de inicio a nuestro muñeco.
        private void ponerCaraNormal()
        {
            OjoDer.Visibility = Visibility.Visible;
            OjoIzq.Visibility = Visibility.Visible;
			OjoIzq.Opacity = 1.0;
            OjoDer.Opacity = 1.0;
            Boca.Visibility = Visibility.Visible;
            OjoDer.Fill = new SolidColorBrush(Colors.White);
            OjoIzq.Fill = new SolidColorBrush(Colors.White);
            
            BocaEnfadado.Visibility = Visibility.Hidden;
            bocaDurmiendo.Visibility = Visibility.Hidden;
            BocaHambre.Visibility = Visibility.Hidden;
            OjoIzqSueno.Visibility = Visibility.Hidden;
            OjoDerSueno.Visibility = Visibility.Hidden;
            OjoIzqDurmiendo.Visibility = Visibility.Hidden;
            OjoDerDurmiendo.Visibility = Visibility.Hidden;
            OjeraIzq.Visibility = Visibility.Hidden;
            OjeraDer.Visibility = Visibility.Hidden;
			OjoIzqMuerto.Visibility = Visibility.Hidden;
			OjoDerMuerto.Visibility = Visibility.Hidden;
        }

        // metodo usado cuando se le da la orden de comer al personaje
        private void empezarComer()
        {
            if (PBapetito.Value > 30 && movimiento_activo == false) // si tiene menos de 30 no tendra hambre
            {

                ponerCaraNormal();
                cara_estado = false;

                come_pizza();
                tReloj.Tick -= new EventHandler(actualizaParametros);
                tReloj.Tick += new EventHandler(comer);
                desactivarTodo();
                
				Boca.Visibility = Visibility.Visible;
				BocaHambre.Visibility = Visibility.Hidden;
				
                texto_hambre = true;

                subirPx(50);
                actualizaMonedas(2);
            }
            else
            {
                diFrase("No tengo hambre");
            }
            
        }

        // metodo usado cuando se le da la orden de dormir al personaje
        private void empezarDormir()
        {
            if(PBsueno.Value > 30 && movimiento_activo == false) // si tiene menos de 30 no tendra sueño
            {
                tReloj.Tick -= new EventHandler(actualizaParametros);
                tReloj.Tick += new EventHandler(dormir);
                desactivarTodo();

                durmiendo = true;

                ponerCaraNormal();
                cara_estado = false;

                pestaneo = null;

                OjoIzq.Opacity = 0.0;
                OjoDer.Opacity = 0.0;
                PupilaIzq.Opacity = 0.0;
                PupilaDer.Opacity = 0.0;
                OjoIzqDurmiendo.Visibility = Visibility.Visible;
                OjoDerDurmiendo.Visibility = Visibility.Visible;
                Boca.Visibility = Visibility.Hidden;
                bocaDurmiendo.Visibility = Visibility.Visible;
                OjoIzqSueno.Visibility = Visibility.Hidden;
                OjoDerSueno.Visibility = Visibility.Hidden;
                OjeraIzq.Visibility = Visibility.Hidden;
                OjeraDer.Visibility = Visibility.Hidden;
                
                CanvasGeneral.IsEnabled = false;

                texto_sueno = true;

                subirPx(50);
                actualizaMonedas(2);
                
            }
            else
            {
                diFrase("No tengo sueño");
            }
            
        }

        // metodo para desactivar todo, ya sea por una animación o por que empieza a dormir o comer, o a jugar al minijuego
        public void desactivarTodo()
        {
            cvIconoPizza.IsEnabled = false;
            CvIconoDormir.IsEnabled = false;
            cvIconoPizza.Opacity = 0.5;
            CvIconoDormir.Opacity = 0.5;

            btnActivarVoz.IsEnabled = false;
            btnDesactivarVoz.IsEnabled = false;
            btnMovimientosEspeciales.IsEnabled = false;
            btnPersonalizacion.IsEnabled = false;
            btnJuego2.IsEnabled = false;
           

            CanvasGeneral.IsEnabled = false;
            GridGeneral.IsEnabled = false;
            cvPerro.IsEnabled = false;
            CvMovimientos_Especiales.Visibility = Visibility.Hidden;
            CvPersonalizacion.Visibility = Visibility.Hidden;

            voz.RecognizeAsyncStop();

        }
        // metodo para activar todo, al final de una animación, de dormir o comer o de jugar al minijuego
        public void activarTodo()
        {
            cvIconoPizza.IsEnabled = true;
            CvIconoDormir.IsEnabled = true;

            if (this.PBapetito.Value > 50)
            {
                cvIconoPizza.Opacity = 1.0;
            }
            else
            {
                cvIconoPizza.Opacity = 0.5;
            }

            if (this.PBsueno.Value > 50)
            {
                CvIconoDormir.Opacity = 1.0;
            }
            else
            {
                CvIconoDormir.Opacity = 0.5;
            }

            btnActivarVoz.IsEnabled = true;
            btnMovimientosEspeciales.IsEnabled = true;
            btnPersonalizacion.IsEnabled = true;
            btnJuego2.IsEnabled = true;
        

            CanvasGeneral.IsEnabled = true;
            GridGeneral.IsEnabled = true;
            cvPerro.IsEnabled = true;

        }

        // metodo para ocultar los 2 grids principales del juego (por ejemplo por que se empieza a jugar al minijuego)
        private void ocultarTodo()
        {
            GridGeneral.Visibility = Visibility.Hidden;
            GridEstado.Visibility = Visibility.Hidden;
        }

        // metodo para mostrar los 2 grids principales del juego 
        private void mostrarTodo()
        {
            GridGeneral.Visibility = Visibility.Visible;
            GridEstado.Visibility = Visibility.Visible;
        }



        //////////////////////////////  Movimientos  //////////////////////////////////////////////////////


        // metodo que hace una animación de abrir la boca (por ejemplo cuando tiene sueño)
        private void AbrirBoca()
        {
            DoubleAnimation moverBoca = new DoubleAnimation();

            moverBoca.From = 19;
            moverBoca.To = 39;
            moverBoca.AutoReverse = true;
            moverBoca.Duration = new Duration(TimeSpan.FromSeconds(2));
            //moverBoca.RepeatBehavior = RepeatBehavior.Forever;
            Boca.BeginAnimation(Path.HeightProperty, moverBoca);
        }

        // metodo para hacer la animacion de que el muñeco vuele
        private void Volar()
        { 
            switch (Dado(2)) // hay 2 tipos de animaciones de volar
            {
                case 1:
                    ThicknessAnimation volarCanvas = new ThicknessAnimation();
                    //volarCanvas.From = new Thickness(left, top, right, bottom);

                    //CORREGIR ESTO
                    volarCanvas.From = CanvasGeneral.Margin;
                    volarCanvas.To = new Thickness(CanvasGeneral.Margin.Left, 0, CanvasGeneral.Margin.Right, 240.333);
                    volarCanvas.AutoReverse = true;
                    volarCanvas.Duration = new Duration(TimeSpan.FromSeconds(1));
                    CanvasGeneral.BeginAnimation(Canvas.MarginProperty, volarCanvas);

                    empezarTemporizadorMovimiento(2000.0); // la animacion dura 2 segundos
                    break;
                case 2:
                    Storyboard moveRight;
                    moveRight = (Storyboard)this.Resources["Vuela2"];
                    moveRight.Begin(this);
					empezarTemporizadorMovimiento(8000.0);
                    break;
            }
			
			subirPx(15); // se suben 15 px
            if (monedas_por_minuto > 0) // si no se le han dado todas las monedas disponibles cada minuto se le dan
            {
                monedas_por_minuto--;
                actualizaMonedas(2);
            }

        }

        // metodo que hace la animacion de pestañear
        private void pestanear()
        {
            pestaneo = (Storyboard)this.Resources["Pestaneo"];
            pestaneo.Begin(this);
        }

        // metodo que hace la animacion de comer pizza
        private void come_pizza()
        {
            Storyboard moveRight;
            moveRight = (Storyboard)this.Resources["Comer"];
            moveRight.Begin(this);
        }


        // metodo que hace la animacion de toques de balon si tiene las monedas suficientes
        private void toquesBalon()
        {
            if (!movimiento_activo)
            {
                if (monedas >= 20)
                {
                    Storyboard moveRight;
                    moveRight = (Storyboard)this.Resources["ToquesBalon"];
                    moveRight.Begin(this);

                    subirPx(100);
                    actualizaMonedas(-20);
                    PBDiversion.Value += 20;
                    

                    empezarTemporizadorMovimiento(7000.0); // la animacion dura 7 segundos
                }
                else
                {
                    diFrase("No tienes las monedas suficientes para que esta animacion");
                    
                }
            }
        }

        // metodo que hace la animacion de bailar si tiene las monedas suficientes
        private void animacionBailar()
        {
            if (!movimiento_activo)
            {
                if (monedas >= 75)
                {
                    Storyboard moveRight;
                    moveRight = (Storyboard)this.Resources["Baile"];
                    moveRight.Begin(this);
                   
                    subirPx(400);
                    actualizaMonedas(-75);
                    PBDiversion.Value += 50;
                    sonido.Stream = Properties.Resources.baile;
                    sonido.Play();
                    empezarTemporizadorMovimiento(20000.0); // la animacion dura unos 20 segundos aproximadamente
                }
                else
                {
                    diFrase("No tienes las monedas suficientes para esta animacion");
                }
            }
        }
        // metodo que hace la animacion de troll si tiene las monedas suficientes
        private void animacionTroll()
        {
          if (!movimiento_activo)
            {
               
                if (monedas >= 50)
                {
                    Storyboard moveRight;
                    moveRight = (Storyboard)this.Resources["Troll"];
                    moveRight.Begin(this);
                  
                    subirPx(300);
                    actualizaMonedas(-50);
                    PBDiversion.Value += 40;

                    sonido.Stream = Properties.Resources.trol;
                    sonido.Play();

                    empezarTemporizadorMovimiento(10000.0); // la animacion dura unos 10 segundos aproximadamente
                }
                else
                {
                    diFrase("No tienes las monedas suficientes para esta animacion");
                }
            }
        }

        // metodo que hace la animacion del champiñon si tiene las monedas suficientes
        private void animacionChampinon()
        {
            if (!movimiento_activo)
            {
                if (monedas >= 100)
                {
                    Storyboard moveRight;
                    moveRight = (Storyboard)this.Resources["TomarChampinon"];
                    moveRight.Begin(this);
                    
                    subirPx(500);
                    actualizaMonedas(-100);

                    empezarTemporizadorMovimiento(6500.0); // la animacion dura unos 6'5 segundos aproximadamente

                    // se restablecen todas las PB
                    PBsueno.Value = 0;
                    PBapetito.Value = 0;
                    PBDiversion.Value = 100;
                    PBenfado.Value = 0;

                    // con esta animacion se restablecen todos los valores de las barras
                    
                }
                else
                {
                    diFrase("No tienes las monedas suficientes para esta animacion");
                }
            }
        }

        // metodo que hace la animacion de las palmas si tiene las monedas suficientes
        private void animacionPalmas()
        {
            if (!movimiento_activo)
            {
                if (monedas >= 10)
                {
                    Storyboard moveRight;
                    moveRight = (Storyboard)this.Resources["TocaPalmas"];
                    moveRight.Begin(this);
					

                    subirPx(75);
                    actualizaMonedas(-10);
                    PBDiversion.Value += 10;

					sonido.Stream = Properties.Resources.cancionpalmas;
            		sonido.Play();
                    empezarTemporizadorMovimiento(6000.0); // la animacion dura 6 segundos
                }
                else
                {
                    diFrase("No tienes las monedas suficientes para esta animacion");
                }
            }
        }

        // metodo que hace la animacion del frisbee si tiene las monedas suficientes
        private void animacionFrisbee()
        {
            if (!movimiento_activo)
            {
                if (monedas >= 35)
                {
                    Storyboard moveRight;
                    moveRight = (Storyboard)this.Resources["PerroFrisbee"];
                    moveRight.Begin(this);

                    subirPx(200);
                    actualizaMonedas(-35);
                    PBDiversion.Value -= 30;

                    empezarTemporizadorMovimiento(12000.0); // la animacion dura 11 segundos aproximadamente
                }
                else
                {
                    //MOSTRAR ERROR, HACER
                    diFrase("No tienes las monedas suficientes para que esta animacion");
                }
            }
        }

        // metodo que hace la animacion de tocarse la tripa
        private void tocaTripa(){
			Storyboard moveRight;
            moveRight = (Storyboard)this.Resources["TocaTripa"];
            moveRight.Begin(this);
		}

        // metodo que hace la animacion de bajarse los pantalones
        private void bajarCalzones()
        {
            subirPx(30);
            ThicknessAnimation mover = new ThicknessAnimation();
            mover.From = Calzones.Margin;

            if (calzones_bajados == false)
            {
                mover.To = new Thickness(0, 0, 0, -328);
                calzones_bajados = true;
                PBenfado.Value += 10;
                OjoDer.Fill = new SolidColorBrush(Colors.Red);
                OjoIzq.Fill = new SolidColorBrush(Colors.Red);
            }
            else
            {
                mover.To = new Thickness(0, 0, 0, 0);
                calzones_bajados = false;
                PBenfado.Value -= 5;
                OjoDer.Fill = new SolidColorBrush(Colors.White);
                OjoIzq.Fill = new SolidColorBrush(Colors.White);
            }

            mover.AutoReverse = false;
            mover.Duration = new Duration(TimeSpan.FromSeconds(1));
            Calzones.BeginAnimation(Canvas.MarginProperty, mover);
        }

        ////////////////////////////// Sonidos ///////////////////////////////////////////////////////////////////////////


        // metodo para que el juego diga una frase 
        private void diFrase(string frase)
        {
            if (texto_libre && subtitulos) // si en los subtitulos no hay ninguna otra frase se ponen en este
            {
                texto_libre = false; // se bloquea el cuadro de dialogo
                Texto.Text = frase; // se establece el texto
				Texto.Visibility = Visibility.Visible;
                //Texto.Opacity = 100;
                tCuadroDialogo.Tick += new EventHandler(cuadroDialogo);
                tCuadroDialogo.Start();
            }

            // dice la frase independientemente de si estan o no activados los subtitulos
            // creamos un hilo ya que se detenía toda la aplicacion mientras decia la frase
            Thread t = new Thread(() => RunInThread(frase));

            t.Start();
            
        }

        private void RunInThread(string frase)
        {
            synthesizer.Speak(frase);
        }



        ////////////////////////////// Voz ///////////////////////////////////////////////////////////////////////////


        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Choices comandos = new Choices(); // objeto en el que añadimos todos los comandos de voz disponibles
            comandos.Add(new string[] { "come", "vuela", "abre google", "saluda","cuenta un chiste","Que tal",
                "Quienes son tus creadores","cuantos años tienes","me quieres","como te llamas", "Como me llamo", "Di mi nombre",
                "Di algo en inglés", "Soy guapo", "Sabes quien es cortana", "Tienes novia", "De donde eres", "Dime algun consejo",
                "Dime una cita célebre", "Dime un refrán", 
                "Balon", "Frisbi", "Frisbe", "Baila", "Troll", "Champiñon", "Toca las palmas", "Aplaude", "Baja pantalones"});
            GrammarBuilder gBuilder = new GrammarBuilder();
            gBuilder.Append(comandos);
            Grammar grammar = new Grammar(gBuilder);
            voz.LoadGrammarAsync(grammar);
            voz.SetInputToDefaultAudioDevice();
            voz.SpeechRecognized += voz_SpeechRecognized;

        }

        

        private void voz_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            // comprobamos mediante un switch cual ha sido el comando introducido
            switch (e.Result.Text) 
            {
                case "come":
                    empezarComer();
                    break;
                case "vuela":
                    Volar();
                    break;
                case "abre google": // DECIR SOLO GOOGLE MEJOR?
                    System.Diagnostics.Process.Start("https://www.google.com/");
                    break;
                case "saluda":
                    diFrase("¡Hola " + nombre + "!");
                    break;
                case "cuenta un chiste":
                   // int numero=Dado(2);
                    switch (Dado(4))
                    {
                        case 1:
                            diFrase("Te voy a hacer una pregunta,si los zombies se deshacen con el pasar del tiempo, ¿son zombiodegradables?");
                            break;
                        case 2:
                            diFrase("-¿Que problema hay en comprarse un bumerán nuevo?-Que hay que tirar el viejo, y tirar el viejo, y tirar el viejo..");
                            break;
                        case 3:
                            diFrase("-Hola,¿está Félix?, -que va, hoy estoy tristex ");
                            break;
                            case 4:
                            diFrase("¿Cual es el mar que hace más daño?. El mar tillo");
                            break;
                    }
                    break;
                case "Que tal":
                    switch (Dado(3))
                    {
                        case 1:
                            diFrase("Y esa pregunta, ¿no ves mi barra de estado? ");
                            break;
                        case 2:
                            diFrase("¿Desde cuando te importa como estoy?");
                            break;
                        case 3:
                            diFrase("Pues..la verdad estoy animado y eso que no soy un ¡dibujo!, pideme que te cuente un chiste que me sé muchos y así te ries, jejej");
                            break;
                    }
                    break;
                case "Quienes son tus creadores":
                    diFrase("Mis creadores se llaman Luis Palencia y Jesús Sanguino, estudiantes de ingeniería Informática en ciudad real.");
                    break;
                case "cuantos años tienes":
                    diFrase("Esa pregunta,es complicada el paso del tiempo pasa distinto donde yo vivo");
                    break;
                case "me quieres":
                    diFrase("En cierta forma tú eres mi razón de existir");
                    break;

                case "como te llamas":
                    diFrase("Soy superEstrellaMan, un gran superheroe para que nunca te aburras");
                    break;
                case "Como me llamo":
                    diFrase("Te llamas " + nombre);
                    break;
                case "Di mi nombre":
                    diFrase("Si no me has engañado, tu nombre es " + nombre);
                    break;
                case "Activa subtitulos":
                    break;
                case "Soy guapo":
                    diFrase("Sigue soñando si");
                    break;
                case "Tienes novia":
                    diFrase("No, todavia no tengo algoritmos para el romance");
                    break;
                case "Sabes quien es cortana":
                    diFrase("Una asistente personal muchisimo peor que yo");
                    break;
                case "De donde eres":
                    diFrase("Con lo guapo que soy no puedo ser del mismo lugar que tu");
                    break;
                case "Dime algun consejo":
                    switch (Dado(3))
                    {
                        case 1:
                            diFrase("Intenta no jugar demasiado y disfrutar más de la vida");
                            break;
                        case 2:
                            diFrase("Duerme 8 horas diarias");
                            break;
                        case 3:
                            diFrase("El mejor consejo es el que se da uno mismo");
                            break;
                    }
                    break;
                case "Dime una cita célebre":
                    switch (Dado(3))
                    {
                        case 1:
                            diFrase("'Vencer y perdonar es vencer 2 veces'. Calderón de la Barca");
                            break;
                        case 2:
                            diFrase("¡Ojala vivas todos los dias de tu vida!' Jonathan Swift");
                            break;
                        case 3:
                            diFrase("'Si es bueno vivir, todavía es mejor soñar, y lo mejor de todo, despertar.' Antonio Machado");
                            break;
                    }
                    break;

                case "Dime un refrán":
                    switch (Dado(8))
                    {
                        case 1:
                            diFrase("La palabra es plata, y el silencio es oro");
                            break;
                        case 2:
                            diFrase("A lo hecho, pecho");
                            break;
                        case 3:
                            diFrase("Agua pasada no mueve molino");
                            break;
                        case 4:
                            diFrase("Al que Dios se la dé, San Pedro se la bendiga");
                            break;
                        case 5:
                            diFrase("Cuanto más vieja, más pelleja");
                            break;
                        case 6:
                            diFrase("Dentro de cien años, todos calvos");
                            break;
                        case 7:
                            diFrase("No es oro todo lo que reluce (menos mi ultimo traje)");
                            break;
                        case 8:
                            diFrase("Quien tiene boca se equivoca");
                            break;
                    }
                    break;
                    //movimientos especiales
                case "Balon":
					toquesBalon();
					break;
				case "Frisbi":
                    animacionFrisbee();
                    break;
                case "Frisbe":
                    animacionFrisbee();
                    break;
                case "baila":
                    animacionBailar();
                    break;
                case "Troll":
                    animacionTroll();
                    break;
                case "Champiñon":
                    animacionChampinon();
                    break;
                case "Toca las palmas":
                    animacionPalmas();
                    break;
                case "Aplaude":
                    animacionPalmas();
                    break;
				case "Baja pantalones":
					 bajarCalzones();
					break;
            }

            subirPx(30);
            if(monedas_por_minuto > 0)
            {
                monedas_por_minuto--;
                actualizaMonedas(1);
            }
        }

        ////////////////////////////// Juegos ///////////////////////////////////////////////////////////////////////////

     
        // oyente del botón del minijuego
        private void btnJuego2_Click(object sender, RoutedEventArgs e)
        {
            ocultarTodo(); // se oculta lo principal de la aplicacion
            cvJuego.Visibility = Visibility.Visible;
            tReloj.Tick -= new EventHandler(actualizaParametros);
            lblTiempoRestante.Text = "Tiempo restante: " + 30; // el juego dura 30 segundos

        }





        // oyente del boton de empezar el juego
        private void btnEmpezarJuego_Click(object sender, RoutedEventArgs e)
        {
            rtbInformacion.Visibility = Visibility.Hidden;
            btnEmpezarJuego.Visibility = Visibility.Hidden;
            rbFacil.Visibility = Visibility.Hidden;
            rbNormal.Visibility = Visibility.Hidden;
            rbDificil.Visibility = Visibility.Hidden;
            cvPerroBueno.Visibility = Visibility.Visible;
            cvPerroMalo.Visibility = Visibility.Visible;

            segundos_restantes = 30;
            lblTiempoRestante.Text = "Tiempo restante: " + segundos_restantes;

            tJuego.Start();

            tJuego.Tick += new EventHandler(temporizadorMoverPerros);
            tReloj.Tick += new EventHandler(temporizadorJuego);

            // se comprueba la dificultad elegida
            if (rbFacil.IsChecked==true)
            {
                tJuego.Interval = TimeSpan.FromMilliseconds(1000.0);
                dificultad_juego = 1;

            }
            if (rbNormal.IsChecked== true)
            {
                tJuego.Interval = TimeSpan.FromMilliseconds(500.0);
                dificultad_juego = 2;

            }
            if (rbDificil.IsChecked==true)
            {
                tJuego.Interval = TimeSpan.FromMilliseconds(250.0);
                dificultad_juego = 3;
            }

        }
        // metodo que se ejecuta cada segundo en el juego
        private void temporizadorJuego(object sender, EventArgs e)
        {
            segundos_restantes--;

            if(segundos_restantes < 0) // el minijuego ya ha terminado
            {
                //acabar juego
            	tJuego.Stop();
                tJuego.Tick -= new EventHandler(temporizadorMoverPerros);
                tReloj.Tick -= new EventHandler(temporizadorJuego);
                txtMensajeFinal.Text = "Has llegado al final del juego"+"\n"+ "Has obtenido"+" " + puntos +" "+ "puntos";
                txtMensajeFinal.Visibility = Visibility.Visible;
                
                // rtbMensajeFinal.Visibility = Visibility.Visible;
                cvPerroBueno.Visibility = Visibility.Hidden;
                cvPerroMalo.Visibility = Visibility.Hidden;
               
                btnVolverJuego.Visibility = Visibility.Visible;
            }
            else // el minijuego todavia no ha terminado
            {
                lblTiempoRestante.Text = "Tiempo restante: " + segundos_restantes;
            }
        }

        private void temporizadorMoverPerros(object sender, EventArgs e)
        {
            moverPerros();
        }

        private void btnSalir(object sender, RoutedEventArgs e)
        {
            
            mostrarTodo();
            cvJuego.Visibility = Visibility.Hidden;

            tJuego.Stop();
            tJuego.Tick -= new EventHandler(temporizadorMoverPerros);
            tReloj.Tick -= new EventHandler(temporizadorJuego);
            cvPerroBueno.Visibility = Visibility.Hidden;
            cvPerroMalo.Visibility = Visibility.Hidden;
            txtMensajeFinal.Visibility = Visibility.Hidden;
            btnEmpezarJuego.Visibility = Visibility.Visible;
            rtbInformacion.Visibility = Visibility.Visible;
            rbFacil.Visibility = Visibility.Visible;
            rbNormal.Visibility = Visibility.Visible;
            rbDificil.Visibility = Visibility.Visible;
            btnVolverJuego.Visibility = Visibility.Hidden;
            puntos = 0;
            lblPuntuacion.Content = "Puntuacion:" + " " + puntos;
            lblTiempoRestante.Text = "Tiempo restante: " + 30;
            actualizaMonedas(-10);
            if (monedas > 0)
            {
                diFrase("Has perdido 10 monedas por abandonar el minijuego");
            }
            diFrase("Como tenías menos de 10 monedas y has abandonado el juego, has perdido todas tus monedas");
			tReloj.Tick += new EventHandler(actualizaParametros);
        }

        private void moverPerros() // metodo para mover los perros de manera aleatoria
        {
            cvPerroBueno.Margin = new Thickness(Dado(800), Dado(284), 0, Dado(284));
            cvPerroMalo.Margin = new Thickness(Dado(810), Dado(420), Dado(700), Dado(284));
          
        }

        // oyente de si se hace click en el perro malo
        private void PerroMalo_click(object sender, MouseButtonEventArgs e)
        {
            sonido.Stream = Properties.Resources.fallo;
            sonido.Play();
            puntos = puntos - 1;
            lblPuntuacion.Content = "Puntuacion:" +" "+ puntos;
        }
        // oyente de si se hace click en el perro bueno
        private void PerroBueno_click(object sender, MouseButtonEventArgs e)
        {
            sonido.Stream = Properties.Resources.acierto;
            sonido.Play();

            puntos = puntos + 1;
            lblPuntuacion.Content = "Puntuacion:" + " "+puntos;
        }
        // oyente del boton volver al juego
        private void btnVolverJuego_Click(object sender, System.Windows.RoutedEventArgs e)
        {
			mostrarTodo();
            cvJuego.Visibility = Visibility.Hidden;
            cvPerroBueno.Visibility = Visibility.Hidden;
            cvPerroMalo.Visibility = Visibility.Hidden;
            btnVolverJuego.Visibility = Visibility.Hidden;
            btnEmpezarJuego.Visibility = Visibility.Visible;
            rtbInformacion.Visibility = Visibility.Visible;
            rtbMensajeFinal.Visibility = Visibility.Hidden;
            rbFacil.Visibility = Visibility.Visible;
            rbNormal.Visibility = Visibility.Visible;
            rbDificil.Visibility = Visibility.Visible;

            
            if (dificultad_juego == 1) //premios nivel facil
            {
                subirPx(100);
                if (puntos > 50)
                {
                    actualizaMonedas(30);
                }

                else if (puntos >= 20 && puntos <= 40)
                {
                    actualizaMonedas(10);
                }

                else if (puntos > 0 && puntos < 20)
                {
                    actualizaMonedas(0);
                }

                else if (puntos < 0)
                {
                    actualizaMonedas(-10);
                }
            }
            else if (dificultad_juego == 2) //premios nivel medio
            {
                subirPx(200);
                if (puntos > 20)
                {
                    actualizaMonedas(30);
                }

                else if (puntos >= 10 && puntos <= 20)
                {
                    actualizaMonedas(20);
                }

                else if (puntos > 0 && puntos < 10)
                {
                    actualizaMonedas(5);
                }

                else if (puntos < 0)
                {
                    actualizaMonedas(-10);
                }
            }
            else if (dificultad_juego == 3) //premios nivel dificil
            {
                subirPx(300);
                if (puntos > 10)
                {
                    actualizaMonedas(50);
                }

                else if (puntos >= 5 && puntos <= 10)
                {
                    actualizaMonedas(25);
                }

                else if (puntos > 0 && puntos < 5)
                {
                    actualizaMonedas(5);
                }

                else if (puntos < 0)
                {
                    actualizaMonedas(-10);
                }
            }

            tReloj.Tick += new EventHandler(actualizaParametros);

            puntos = 0;
            lblPuntuacion.Content = "Puntuacion:" + " " + puntos;
            txtMensajeFinal.Visibility = Visibility.Hidden;

        }//fin metodo
    }
} 

