﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PracticaFinal
{
    class Estado
    {
        int energia;
        public int Energia
        {
            get { return energia; }
            set { energia = value; }
        }

        int apetito;
        public int Apetito
        {
            get { return apetito; }
            set { apetito = value; }
        }
        int enfado;
        public int Enfado
        {
            get { return enfado; }
            set { enfado = value; }
        }

        public Estado(int energia, int apetito, int enfado)
        {
            this.apetito = apetito;
            this.energia = energia;
            this.enfado = enfado;
        }
    }
}
